const BillSplitter = require('./src/bill-split.js');

const PEOPLE = 3;
const TOTAL_BILL = 1100;

const billPerPerson = BillSplitter.split(TOTAL_BILL, PEOPLE);

console.log('INR %d per person', billPerPerson);
